function addTokens(input, tokens) {

    if (typeof input == 'string') {

        if (input.length >= 6) {
            if (input.indexOf("...") != -1) {
                let modified = input;

                for (let i = 0; i < tokens.length; i++) {

                    modified = modified.replace("...", '${' + tokens[i].tokenName + '}')
                }
                return modified

            } else {
                return input
            }
            // let a = 0
            // for (let j = 0; j < tokens.length; j++) {
            //     if ((typeof tokens[j].tokenName == 'string')) {
            //         a++
            //     }
            // }
            // if (a != tokens.length) {
            //     throw new Error('Invalid array format');
            // }
        } else {
            throw new Error('Input should have at least 6 characters!');
        }

    } else {
        throw new Error('Invalid input');
    }

}

const app = {
    addTokens: addTokens
}

module.exports = app;